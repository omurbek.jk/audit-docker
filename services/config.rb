coreo_agent_selector_rule 'check-docker' do
    action :define
    timeout 30
    control 'check-docker' do
        describe command('docker') do
            it { should exist }
        end
    end
end

coreo_agent_audit_profile 'cis-docker-benchmark' do
    action :define
    selectors ['check-docker']
    profile 'https://github.com/dev-sec/cis-docker-benchmark/archive/master.zip'
    timeout 120
end

coreo_agent_rule_runner 'audit-docker-profiles' do
    action :run
    profiles ${AUDIT_DOCKER_PROFILES_ALERT_LIST}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
  
