Audit CIS Benchmark Docker Applicaiton
============================
This stack will perform CIS Benchmark Docker application audits

## Description
This composite will audit docker application for Docker CIS Benchmark

* ![DevSec CIS Docker Benchmark](https://github.com/dev-sec/cis-docker-benchmark "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_DOCKER_PROFILES_ALERT_LIST`:
  * description: 
  * default: docker-cis


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices
1. CIS

## Categories


## Diagram


## Icon


